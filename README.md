# README #

## Overview
X-CAP is a pathogenicity predictor for stopgain mutations.

## Repo organization
* ``data/``: Contains the set of variants in $$\mathcal{D}_{\text{original}}$$ and $$\mathcal{D}_{\text{validation}}$$ in VCF files. Variants from HGMD are only labeled with their HGMD accession number.
